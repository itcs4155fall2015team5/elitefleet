var express = require('express');
var path = require('path');
var app = express.Router();
var fs = require('fs');
var busboy = require('connect-busboy');
var formidable = require('formidable');
app.use(busboy());
var appDir = path.dirname(require.main.filename);

var MongoClient = require('bluebird').promisifyAll(require('mongodb')).MongoClient;
var databaseUrl = "mongodb://heroku_94t88551:6pufg10n5ar44urd9plbd0kv8t@ds057244.mongolab.com:57244/heroku_94t88551"; // "username:password@example.com/mydb"
var db = require("mongojs");

/* GET home page. */
app.get('/', function(req, res) {
  loadIndex(res);
});

app.get('/upload', function(req,res){
    res.render('upload.jade', {title: 'Upload'});
});

app.get('/test', function(req,res){
    loadTest(res);
});

app.get('/getvehicles', function(req, res){
    console.log("rows:" + req.param('rows'));
    console.log("offset:" + req.param('offset'));
    getVehicles(res,{ });
});

app.post('/getvehicles', function(req, res){
    console.log("rows:" + req.param('rows'));
    console.log("offset:" + req.param('offset'));
    getVehicles(res,{ });
});

app.get('/listvehicles', function(req, res){
   res.redirect('/vehiclelist');
});




insertVehiclesFromCSV = function (fileName) {
   

    MongoClient.connect(databaseUrl,function(err, db){
            console.log(err);

            var stream = fs.createReadStream(fileName, {flags: 'r', encoding: 'utf-8'});
            var buf = '';

            stream.on('data', function (d) {
                buf += d.toString(); // when data is read, stash it in a string buffer
                pump(); // then process the buffer
            });


            function pump() {
                var pos;

                while ((pos = buf.indexOf('\n')) >= 0) { // keep going while there's a newline somewhere in the buffer
                    if (pos == 0) { // if there's more than one newline in a row, the buffer will now start with a newline
                        buf = buf.slice(1); // discard it
                        continue; // so that the next iteration will start with data
                    }
                    processLine(buf.slice(0, pos)); // hand off the line
                    buf = buf.slice(pos + 1); // and slice the processed data off the buffer
                }
            };
            function processLine(line) { // here's where we do something with a line

                if (line[line.length - 1] == '\r') line = line.substr(0, line.length - 1); // discard CR (0x0D)

                if (line.length > 0) { // ignore empty lines


                    var currentPosition = 0;
                    var details = new Array;
                    var length = 0;
                    //loop 33  times, then once untill undefined (end of line)
                    do {
                        if (line[currentPosition] == ',') {
                            details.push(' ');
                        }
                        else {
                            do {
                                length++;
                                currentPosition++;
                            } while (line[currentPosition] != ',' && line[currentPosition] != undefined);
                            details.push(line.substring(currentPosition - length + 1, currentPosition));
                        }
                        currentPosition++;
                        length = 1;

                    } while (line[currentPosition] != undefined);

                    var vehicle = {
                        'vehicle_id': details[0].replace(/["']/g, ""),
                        'vehicle_class': details[1].replace(/["']/g, ""),
                        'vehicle_year': details[2].replace(/["']/g, ""),
                        'vehicle_make': details[3].replace(/["']/g, ""),
                        'vehicle_model': details[4].replace(/["']/g, ""),
                        'vehicle_status': details[5].replace(/["']/g, ""),
                        'vehicle_workorder': details[6].replace(/["']/g, ""),
                        //labor_shop
                        //sublet_shop
                        //part_shop
                        'vehicle_workorder_date_in': details[10].replace(/["']/g, ""),
                        'vehicle_workorder_date_out': details[11].replace(/["']/g, ""),
                        //workorder_shop
                        'vehicle_transaction_date': details[13].replace(/["']/g, ""),
                        'vehicle_repair_code': details[14].replace(/["']/g, ""),
                        //part_index
                        //parts_qty_issued,
                        //column r,
                        //in_shop_hours
                        //asset_created_date
                        'vehicle_repair_type_desc': details[21].replace(/["']/g, ""),
                        'vehicle_repair_group': details[22].replace(/["']/g, ""),
                        'vehicle_repair_component': details[23].replace(/["']/g, ""),
                        'vehicle_repair_action': details[24].replace(/["']/g, ""),
                        //vendor_code
                        //vendor_name
                        //vendor_invoice
                        //asset_shop
                        'vehicle_asset_group': details[29].replace(/["']/g, ""),
                        'vehicle_fiscal_year': details[30].replace(/["']/g, ""),
                        //part_number
                        //part_name
                        'vehicle_repair_desc': details[33].replace(/["']/g, "")
                    };

                     db.collection('vehicles').insert(vehicle,function (err, result) {
                               // console.log("A vehicle has just been inserted into the database");
                            });

                }
            }
     });
};




app.post('/uploadCSV', function(req, res){

        var fstream;

        req.pipe(req.busboy);
        console.log("Preparing upload:");
        req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype){
            console.log("Found file: " + filename);
            if(filename == "") //fun fact, the name is an empty string, not null, if no file is selected
            {
                res.redirect('/upload');
            }
            else
            {
                console.log("Uploading: " + filename);
                fstream = fs.createWriteStream(__dirname + '/../upload/' + filename);
                console.log(__dirname + '/../upload/' + filename);
                file.pipe(fstream);
                fstream.on('close', function () {
                    insertVehiclesFromCSV(__dirname + '/../upload/' + filename);
                    res.redirect('/'); 
                });
            }

        });
       
});

app.get('/newvehicle', function(req, res){
	res.render('newvehicle.jade', {title:'New Vehicle'});
});

var findvehicles = function(db, callback){
    var cursor = db.collection('vehicles').find();
    cursor.each(function(err, doc){
        console.log('in vehicles');
        if(doc != null){

            console.dir(doc);
        }
        else{
            callback();
        }
    });


};

app.get('/vehiclelist', function(req, res){

    loadVehicles(res);

});


//TODO add false vehicle data trapping
app.post('/newvehicle', function(req, res) {

    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (err) {

            // Check for and handle any errors here.

            console.error(err.message);
        }

        var vehicle = {
            'vehicle_id': fields.vehicle_id,
            'vehicle_class': fields.vehicle_class,
            'vehicle_year': fields.vehicle_year,
            'vehicle_make': fields.vehicle_make,
            'vehicle_model': fields.vehicle_model,
            'vehicle_status': fields.vehicle_status,
            'vehicle_workorder': fields.vehicle_status,
            'vehicle_workorder_date_in': fields.vehicle_workorder_date_in,
            'vehicle_workorder_date_out': fields.vehicle_workorder_date_out,
            'vehicle_transaction_date': fields.vehicle_transaction_date,
            'vehicle_repair_code': fields.vehicle_repair_code,
            'vehicle_repair_type_desc': fields.vehicle_repair_type_desc,
            'vehicle_repair_group': fields.vehicle_repair_group,
            'vehicle_repair_component': fields.vehicle_repair_component,
            'vehicle_repair_action': fields.vehicle_repair_action,
            'vehicle_asset_group': fields.vehicle_asset_group,
            'vehicle_fiscal_year': fields.vehicle_fiscal_year,
            'vehicle_repair_desc': fields.vehicle_repair_desc

        };

        insertVehicle(vehicle);

        res.redirect('vehiclelist');
    });

});


module.exports = app;
