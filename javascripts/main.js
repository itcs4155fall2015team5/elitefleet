
doSearch = function(field){


}

loadHomeStart = function(){
  var dg = $('#dg');
  
    dg.datagrid({
      fit:true,
      singleSelect:true,
      remoteSort:true,
      remoteFilter:true,
      toolbar:'#tb',
      url:'/getVehicles' ,
      rownumbers:true, 
      pagination:true,
      pageSize:20,
      striped:true,
      limit:300,
       columns:[[
           {field:'vehicle_id',title:'ID',align:'center',sortable:true},
           {field:'vehicle_class',title:'Class',align:'center',sortable:true},
           {field:'vehicle_make',title:'Make',align:'center',sortable:true},
           {field:'vehicle_model',title:'Model',align:'center',sortable:true},
           {field:'vehicle_year',title:'Year',align:'center',sortable:true},
           {field:'vehicle_status',title:'Status',align:'center',sortable:true}
       ]]
    });

    dg.datagrid('enableFilter');  
   // dg.datagrid('reload');
}


loadHome = function(){
  var dg = $('#dg');
  
    dg.datagrid({
       columns:[[
           {field:'vehicle_id',title:'ID',align:'center',sortable:true},
           {field:'vehicle_class',title:'Class',align:'center',sortable:true},
           {field:'vehicle_make',title:'Make',align:'center',sortable:true},
           {field:'vehicle_model',title:'Model',align:'center',sortable:true},
           {field:'vehicle_year',title:'Year',align:'center',sortable:true},
           {field:'vehicle_status',title:'Status',align:'center',sortable:true}
       ]]
    });

   // dg.datagrid('reload');
}

loadUploadTest = function(){
    var dg = $('#dg');
  
    dg.datagrid({
       url:'/readJson',
       columns:[[
           {field:'vehicle_id',title:'ID',align:'center',sortable:true},
           {field:'vehicle_class',title:'Class',align:'center',sortable:true},
           {field:'vehicle_make',title:'Make',align:'center',sortable:true},
           {field:'vehicle_model',title:'Model',align:'center',sortable:true},
           {field:'vehicle_year',title:'Year',align:'center',sortable:true},
           {field:'vehicle_status',title:'Status',align:'center',sortable:true}
       ]]
    });


    
   // dg.datagrid('reload');
}
	
$(document).ready(function(){ 
    loadHomeStart();
});


loadWorkorders = function(){
    var dg = $('#dg');

    dg.datagrid({
       columns:[[
           {field:'vehicle_workorder',title:'Workorder',align:'center',sortable:true},
           {field:'vehicle_labor_shop',title:'Labor Shop',align:'center',sortable:true},
           {field:'vehicle_sublet_shop',title:'Sublet Shop',align:'center',sortable:true},
           {field:'vehicle_part_shop',title:'Part Shop',align:'center',sortable:true},
           {field:'vehicle_workorder_date_in',title:'Workorder Date In',align:'center',sortable:true},
           {field:'vehicle_workorder_date_out',title:'Workorder Date Out',align:'center',sortable:true},
           {field:'vehicle_workorder_shop',title:'Workorder Shop',align:'center',sortable:true},
           {field:'vehicle_transaction_date',title:'Transaction Date',align:'center',sortable:true},
           {field:'vehicle_repair_code',title:'Repair Code',align:'center',sortable:true}
        ]]
    });

  //  dg.datagrid('reload');
}


loadParts = function(){
    var dg = $('#dg');

    dg.datagrid({
       columns:[[
           {field:'vehicle_workorder',title:'Workorder',align:'center',sortable:true},
           {field:'vehicle_labor_shop',title:'Labor Shop',align:'center',sortable:true},
           {field:'vehicle_sublet_shop',title:'Sublet Shop',align:'center',sortable:true},
           {field:'vehicle_part_shop',title:'Part Shop',align:'center',sortable:true},
           {field:'vehicle_workorder_shop',title:'Workorder Shop',align:'center',sortable:true},
           {field:'vehicle_transaction_date',title:'Transaction Date',align:'center',sortable:true},
           {field:'vehicle_part_index',title:'Part Index',align:'center',sortable:true},
           {field:'vehicle_parts_qty_issued',title:'Part Quantity Issued',align:'center',sortable:true}
        ]]
    });

   // dg.datagrid('reload');
}





