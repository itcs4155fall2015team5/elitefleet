var appRoot = require('app-root-path');
var path = require('path');
var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var monk = require('monk');
var formidable = require('formidable');
var busboy = require('connect-busboy');


if((typeof alert) === 'undefined') {
    global.alert = function(message) {
        console.log(message);
    }
}


var http = require('http');
var util = require('util');

var jquery = require('jquery');

var MongoClient = require('bluebird').promisifyAll(require('mongodb')).MongoClient;
var ObjectId = require('mongodb').ObjectId

//TODO figure out why heroku can't 'get'

var routes = require('./routes/index');
var users = require('./routes/users');
var app = express();


// app.js
var databaseUrl = "mongodb://heroku_94t88551:6pufg10n5ar44urd9plbd0kv8t@ds057244.mongolab.com:57244/heroku_94t88551"; // "username:password@example.com/mydb"
var db = require("mongojs");


var port = process.env.PORT || 3000;


app.listen(port, function() {
    console.log('Node app is running on port', port);
});

// view engine setup
app.set('views', appRoot + '/views');
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use(cookieParser());
app.use(express.static(appRoot + '/'));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});





insertVehicle = function(data)
{
    MongoClient.connect(databaseUrl, function(err, db){
        db.collection('vehicles').insertOne(
            {
                'vehicle_id' : data.vehicle_id,
                'vehicle_class' : data.vehicle_class,
                'vehicle_year' : data.vehicle_year,
                'vehicle_make' : data.vehicle_make,
                'vehicle_model' : data.vehicle_model,
                'vehicle_status' : data.vehicle_status,
                'vehicle_workorder' : data.vehicle_status,
                'vehicle_workorder_date_in' : data.vehicle_workorder_date_in,
                'vehicle_workorder_date_out' : data.vehicle_workorder_date_out,
                'vehicle_transaction_date' : data.vehicle_transaction_date,
                'vehicle_repair_code' : data.vehicle_repair_code,
                'vehicle_repair_type_desc' : data.vehicle_repair_type_desc,
                'vehicle_repair_group' : data.vehicle_repair_group,
                'vehicle_repair_component' : data.vehicle_repair_component,
                'vehicle_repair_action' : data.vehicle_repair_action,
                'vehicle_asset_group' : data.vehicle_asset_group,
                'vehicle_fiscal_year' : data.vehicle_fiscal_year,
                'vehicle_repair_desc' : data.vehicle_repair_desc
            }, function (err, result) {
                console.log("A vehicle has just been inserted into the database");
            });
    });
};

//load vehicles asynchronously
getVehicles = function(res,constraints){
        var temp;
        MongoClient.connectAsync(databaseUrl)
        .then(function(db) {
           db.collection('vehicles').findAsync(constraints)
                        .then(function(cursor){
                           // cursor.limit(20);
                            return cursor.toArrayAsync();
                        })
                        .then(function(arrayOfVehciles) {
                           res.json(arrayOfVehciles);
                        });
        });

};

//load vehicles asynchronously
loadVehicles = function(res){
    MongoClient.connectAsync(databaseUrl)
        .then(function(db) {
            db.collection('vehicles').findAsync({ })
                .then(function(cursor) {
                    return cursor.toArrayAsync();
                })
                .then(function(arrayOfVehciles) {
                   // console.log(arrayOfVehciles);
                    res.render('vehiclelist.jade',
                        {title: 'Vehicle List',
                            'vehiclelist':arrayOfVehciles});
                });
        });

};

loadIndex = function(res){
    MongoClient.connectAsync(databaseUrl)
        .then(function(db) {
            db.collection('vehicles').findAsync({ })
                .then(function(cursor) {
                    return cursor.toArrayAsync();
                })
                .then(function(arrayOfVehciles) {
                    // console.log(arrayOfVehciles);
                    res.render('index.jade',
                        {title: 'Elite Fleet',
                            'vehiclelist':arrayOfVehciles});
                });
        });

};

loadTest = function(res){
    MongoClient.connectAsync(databaseUrl)
        .then(function(db) {
            db.collection('vehicles').findAsync({ })
                .then(function(cursor) {
                    return cursor.toArrayAsync();
                })
                .then(function(arrayOfVehciles) {
                    // console.log(arrayOfVehciles);
                    res.render('test.jade',
                        {title: 'Elite Fleet',
                            'vehiclelist':arrayOfVehciles});
                });
        });

};


module.exports = app;
